#include "esh.h"

#include <stdio.h>
#include <string.h>

typedef struct Esh {
    uint8_t* buf;
    size_t capacity;
    size_t offset;
    ReadCharFn read_char;
} Esh;

const char help[] = "help";

Esh* esh = NULL;

void cmd_help() { printf("Executing help()\n"); }

EshResult Esh_init(Esh* const self, uint8_t* const buf, const size_t capacity,
                   ReadCharFn read_char) {
    if (self == NULL || buf == NULL || capacity == 0 || read_char == NULL) return EshResultErr;

    self->buf = buf;
    self->capacity = capacity;
    self->offset = 0;
    self->read_char = read_char;

    esh = self;

    return EshResultOk;
}

EshResult esh_run() {
    if (strncmp(esh->buf, help, sizeof(help)) == 0) {
        cmd_help();
        return EshResultOk;
    } else {
        return EshResultCmdNotFound;
    }
}

EshResult esh_read_line() {
    if (esh == NULL) return EshResultNull;

    EshResult result = EshResultOk;
    esh->offset = 0;
    uint8_t ch;

    while (1) {
        result = esh->read_char(&ch);
        if (result != EshResultOk) {
            return result;
        }

        // Handle backspace.
        if (ch == '\b') {
            if (esh->offset > 0) {
                esh->offset--;
            }
        } else if ((ch == '\n' || ch == '\r') || (esh->offset == (esh->capacity - 1))) {
            esh->buf[esh->offset] = '\0';

            return result;
        } else if (ch >= 32 && ch < 127) {
            esh->buf[esh->offset] = ch;
            esh->offset++;
        }
    }
}
