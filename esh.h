#pragma once

#include <stddef.h>
#include <stdint.h>

typedef enum EshResult {
    EshResultOk,
    EshResultErr,
    EshResultNull,
    EshResultCmdNotFound,
    EshResultUnknown,
} EshResult;

typedef struct Esh Esh;

typedef EshResult (*ReadCharFn)(uint8_t* const ch);

EshResult Esh_init(Esh* const self, uint8_t* const buf, const size_t capacity, ReadCharFn read_char);
EshResult esh_read_line();
